###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Midterm
#
# @file    Makefile
# @version 1.0
#
# @brief  Midterm - EE 205 - Spr 2021
###############################################################################

all: main

main:  main.c numbers.c numbers.h
	gcc -o main main.c numbers.c

clean:
	rm -f *.o main
